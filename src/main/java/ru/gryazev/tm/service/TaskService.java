package ru.gryazev.tm.service;

import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.ArrayUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class TaskService{

    private TaskRepository taskRepository;

    private ProjectRepository projectRepository;

    private ProjectService projectService;

    public TaskService(ProjectService projectService) {
        this.projectService = projectService;
        this.taskRepository = projectService.getTaskRepository();
        this.projectRepository = projectService.getProjectRepository();
    }

    public void create(Task task) throws IOException {
        String projectId = projectService.getSelectedProjectId();
        if (!"".equals(projectId))
            task.setProjectId(projectRepository.findOne(projectId).getId());
        taskRepository.persist(task);
    }

    public Task view(String taskId) {
        return taskRepository.findOne(taskId);
    }

    public List<Task> list() {
        String projectId = projectService.getSelectedProjectId();
        return taskRepository.findAll().values().stream().filter(o ->
                o.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    public void edit(Task task) {
        taskRepository.merge(task);
    }

    public boolean remove(String taskId) {
        return (taskRepository.remove(taskId) != null);
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public String getTaskId(int taskIndex) {
        String projectId = projectService.getSelectedProjectId();

        List<Task> tasks = taskRepository.findAll().values().stream().filter(o ->
                o.getProjectId().equals(projectId))
                .collect(Collectors.toList());

        if (!ArrayUtils.indexExists(tasks, taskIndex))
            return null;

        return tasks.get(taskIndex).getId();
    }

}