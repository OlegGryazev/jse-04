package ru.gryazev.tm.enumerated;

public enum ConsoleCommand {

    PROJECT_CREATE("project-create: Create new project."),
    PROJECT_LIST("project-list: Show all projects."),
    PROJECT_REMOVE("project-remove: Remove selected project."),
    PROJECT_EDIT("project-edit: Edit selected project."),
    PROJECT_VIEW("project-view: View selected project."),
    PROJECT_SELECT("project-select: Select project. To reset type \"-1\""),
    PROJECT_CLEAR("project-clear: Clear projects list"),
    TASK_CREATE("task-create: Create new task at selected project."),
    TASK_LIST("task-list: Show all tasks of selected project."),
    TASK_REMOVE("task-remove: Remove selected task from selected project."),
    TASK_EDIT("task-edit: Edit selected task."),
    TASK_VIEW("task-view: View selected task."),
    TASK_CLEAR("task-clear: Clear tasks list"),
    HELP("help: Show all commands."),
    EXIT("exit: Exit from program.");

    private String description;

    ConsoleCommand(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
