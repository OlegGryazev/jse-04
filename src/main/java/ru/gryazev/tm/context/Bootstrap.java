package ru.gryazev.tm.context;

import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.enumerated.ConsoleCommand;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Bootstrap {

    private ConsoleView consoleView = new ConsoleView();

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private TaskService taskService = new TaskService(projectService);

    public void init() {
        consoleView.print("**** Welcome to Project Manager ****");
        try{
            while (true) {
                ConsoleCommand consoleCommand = consoleView.readCommand();
                if (consoleCommand == null)
                    continue;

                if (consoleCommand == ConsoleCommand.EXIT)
                    return;

                executeCommand(consoleCommand);
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            consoleView.close();
        }
    }

    private void executeCommand(ConsoleCommand consoleCommand) throws IOException {
        switch (consoleCommand) {
            case PROJECT_CREATE: projectCreate();
                break;
            case PROJECT_LIST: projectList();
                break;
            case PROJECT_EDIT: projectEdit();
                break;
            case PROJECT_VIEW: projectView();
                break;
            case PROJECT_SELECT: projectSelect();
                break;
            case PROJECT_REMOVE: projectRemove();
                break;
            case PROJECT_CLEAR: projectService.clear();
                break;
            case TASK_CREATE: taskCreate();
                break;
            case TASK_VIEW: taskView();
                break;
            case TASK_LIST: taskList();
                break;
            case TASK_REMOVE: taskRemove();
                break;
            case TASK_EDIT: taskEdit();
                break;
            case TASK_CLEAR: taskService.clear();
                break;
            case HELP: showHelp();
                break;
            case EXIT: return;
            default:
                consoleView.print("Command not found.");
        }
    }

    private void projectCreate() throws IOException {
        consoleView.print("[PROJECT CREATE]");
        Project project = consoleView.getProjectFromConsole();
        projectService.create(project);
        consoleView.print("[OK]");
    }

    private void projectList() {
        List<Project> projects = projectService.list();
        if (projects.size() == 0){
            consoleView.print("[LIST IS EMPTY]");
        }
        for (int i = 0; i < projects.size(); i++)
            consoleView.print(i + ". " + projects.get(i).getName());
    }

    private void projectEdit() throws IOException {
        Project project = consoleView.getProjectFromConsole();
        projectService.edit(project);
        consoleView.print("[OK]");
    }

    private void projectView() {
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        Project project = projectService.view(projectId);
        consoleView.print(project == null ? "[PROJECT NOT FOUND]" : project.toString());
    }

    private void projectSelect() {
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        projectService.select(projectId);
    }

    private void projectRemove() {
        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);
        if (projectService.remove(projectId))
            consoleView.print("[DELETED]");
    }

    private void taskCreate() throws IOException {
        consoleView.print("[TASK CREATE]");
        Task task = consoleView.getTaskFromConsole();
        taskService.create(task);
        consoleView.print("[OK]");
    }

    private void taskView() {
        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(taskIndex);
        Task task = taskService.view(taskId);
        consoleView.print(task == null ? "[TASK NOT FOUND]" : task.toString());
    }

    private void taskList() {
        List<Task> tasks = taskService.list();
        for (int i = 0; i < tasks.size(); i++)
            consoleView.print(i + ". " + tasks.get(i).getName());
    }

    private void taskRemove() {
        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(taskIndex);
        if (taskService.remove(taskId))
            consoleView.print("[DELETED]");
    }

    private void taskEdit() throws IOException {
        int taskIndex = consoleView.getTaskIndex();
        String taskId = taskService.getTaskId(taskIndex);

        int projectIndex = consoleView.getProjectIndex();
        String projectId = projectService.getProjectId(projectIndex);

        Task task = consoleView.getTaskFromConsole();
        task.setId(taskId);
        task.setProjectId(projectId);

        taskService.edit(task);
        consoleView.print("[OK]");
    }

    private void showHelp() {
        Arrays.stream(ConsoleCommand.values()).forEach(o -> System.out.println(o.getDescription()));
    }

}
